from django.shortcuts import render, HttpResponse
from django.db.models import Count
from .models import *

# Create your views here.
def index(request):

    # Elige el apellido y la edad más repetida
    cuenta_apellidos = Estudiante.objects.values('apellidos').annotate(cuenta=Count('apellidos')).order_by('-cuenta')[0]
    cuenta_edad = Estudiante.objects.values('edad').annotate(cuenta=Count('edad')).order_by('-cuenta')[0]

    grupo1 = Estudiante.objects.filter(grupo = 1)
    grupo4 = Estudiante.objects.filter(grupo = 4)
    mismo_apellido = Estudiante.objects.filter(apellidos = cuenta_apellidos.get('apellidos'))
    misma_edad = Estudiante.objects.filter(edad = cuenta_edad.get('edad'))
    misma_edad3 = misma_edad.filter(grupo = 3)
    todos = Estudiante.objects.all()

    return render(request, 'index.html', {'grupo1': grupo1, 
                                          'grupo4': grupo4, 
                                          'mismo_apellido': mismo_apellido,
                                          'misma_edad': misma_edad,
                                          'misma_edad3': misma_edad3,
                                          'todos': todos})
